import path from 'path'
import initStoryshots from '@storybook/addon-storyshots'
import { imageSnapshot } from '@storybook/addon-storyshots-puppeteer'
import expect from 'expect'
const { configureToMatchImageSnapshot } = require('jest-image-snapshot')

const getMatchOptions = () => {
  return {
    failureThreshold: 0.2,
    failureThresholdType: 'percent'
  }
}

const beforeScreenshot = (page, { context: { kind, story }, url }) => {
  console.log(url)
  return new Promise((resolve) =>
    setTimeout(() => {
      resolve()
    }, 600)
  )
}
initStoryshots({
  framework: 'vue',
  suite: 'storyshots-puppeteer',
  test: imageSnapshot({
    getMatchOptions,
    beforeScreenshot,
    storybookUrl: process.env.CI ? `file:///${path.resolve(__dirname, '../../storybook-static')}` : 'http://localhost:6006'
  })
})

// https://github.com/americanexpress/jest-image-snapshot#optional-configuration
const toMatchImageSnapshot = configureToMatchImageSnapshot({
  customDiffConfig: { threshold: 0 }
})

expect.extend({ toMatchImageSnapshot })
