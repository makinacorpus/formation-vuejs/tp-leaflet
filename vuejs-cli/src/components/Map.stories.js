import { action } from '@storybook/addon-actions'
import Map from './Map.vue'

export default {
  component: Map,
  title: 'components/Map'
}

export const defaultStory = () => ({
  components: { Map },
  template: '<Map />'
})

export const withData = (args) => ({
  data () {
    return {
      ...args
    }
  },
  components: { Map },
  template: `
<Map
  :nominatimResult="nominatimResult"
  :current-marker="selected"
  @bbox="onBbox"
>
  <template slot="popup">
    {{ selected.display_name }}
  </template>
</Map>
`,
  methods: {
    onBbox: action('bbox')
  }
})
withData.args = {
  nominatimResult: [{
    place_id: 258132802,
    licence: 'Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright',
    osm_type: 'relation',
    osm_id: 11148976,
    boundingbox: [
      '43.6037617',
      '43.6050273',
      '1.4426999',
      '1.4440579'
    ],
    lat: '43.60439865',
    lon: '1.443351576181684',
    display_name: 'Place du Capitole, Place Occitane, Capitole, Toulouse Centre, Toulouse, Haute-Garonne, Occitanie, France métropolitaine, 31000, France',
    class: 'highway',
    type: 'pedestrian',
    importance: 0.82190327427879
  }, {
    place_id: 99969528,
    licence: 'Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright',
    osm_type: 'way',
    osm_id: 38250216,
    boundingbox: [
      '43.6039272',
      '43.6039345',
      '1.4438921',
      '1.4440478'
    ],
    lat: '43.6039322',
    lon: '1.44394',
    display_name: 'Place du Capitole, Place Occitane, Capitole, Toulouse Centre, Toulouse, Haute-Garonne, Occitanie, France métropolitaine, 31000, France',
    class: 'highway',
    type: 'living_street',
    importance: 0.6299999999999999
  }, {
    place_id: 85381473,
    licence: 'Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright',
    osm_type: 'way',
    osm_id: 7814172,
    boundingbox: [
      '43.6038447',
      '43.6039345',
      '1.44309',
      '1.4438921'
    ],
    lat: '43.6038889',
    lon: '1.4434851',
    display_name: 'Place du Capitole, Place Occitane, Capitole, Toulouse Centre, Toulouse, Haute-Garonne, Occitanie, France métropolitaine, 31000, France',
    class: 'highway',
    type: 'pedestrian',
    importance: 0.6299999999999999
  }, {
    place_id: 52339770,
    licence: 'Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright',
    osm_type: 'node',
    osm_id: 4406405689,
    boundingbox: [
      '43.6043201',
      '43.6044201',
      '1.4433102',
      '1.4434102'
    ],
    lat: '43.6043701',
    lon: '1.4433602',
    display_name: 'Croix occitane et ses 12 signes du zodiaque, Place du Capitole, Place Occitane, Capitole, Toulouse Centre, Toulouse, Haute-Garonne, Occitanie, France métropolitaine, 31000, France',
    class: 'tourism',
    type: 'artwork',
    importance: 0.5309999999999999,
    icon: 'https://nominatim.openstreetmap.org/ui/mapicons//tourist_art_gallery2.p.20.png'
  }],
  selected: {
    place_id: 52339770,
    licence: 'Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright',
    osm_type: 'node',
    osm_id: 4406405689,
    boundingbox: [
      '43.6043201',
      '43.6044201',
      '1.4433102',
      '1.4434102'
    ],
    lat: '43.6043701',
    lon: '1.4433602',
    display_name: 'Croix occitane et ses 12 signes du zodiaque, Place du Capitole, Place Occitane, Capitole, Toulouse Centre, Toulouse, Haute-Garonne, Occitanie, France métropolitaine, 31000, France',
    class: 'tourism',
    type: 'artwork',
    importance: 0.5309999999999999,
    icon: 'https://nominatim.openstreetmap.org/ui/mapicons//tourist_art_gallery2.p.20.png'
  }
}
