import { action } from '@storybook/addon-actions'
import Address from './Address.vue'

export default {
  component: Address,
  title: 'components/Address'
}

export const defaultStory = () => ({
  components: { Address },
  template: '<Address />'
})

export const withData = (args) => ({
  data () {
    return {
      ...args
    }
  },
  components: { Address },
  methods: {
    onEdit: action('edit'),
    onUpdate: action('update')
  },
  template: '<Address :address="address" :name="name" @edit="onEdit" @search="onUpdate" />'
})
withData.args = {
  address: {
    city: 'Toulouse',
    road: 'here !',
    zipCode: '31000',
    state: 'France'
  },
  name: 'Testing address'
}
