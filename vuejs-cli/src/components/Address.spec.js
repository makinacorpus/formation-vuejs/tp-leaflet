import { shallowMount } from '@vue/test-utils'
import Address from '@/components/Address.vue'
import Vuetify from 'vuetify'
import Vue from 'vue'
Vue.use(Vuetify)

describe('Address.vue', () => {
  it('renders props.msg when passed', () => {
    const address = {
      road: '52 rue Jacques Babinet',
      zipCode: '31000',
      city: 'Toulouse'
    }
    const wrapper = shallowMount(Address, {
      propsData: { address }
    })
    expect(wrapper.html()).toMatchSnapshot()
  })
})
