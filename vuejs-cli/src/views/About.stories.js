import About from './About.vue'

export default {
  component: About,
  title: 'views/About'
}

export const defaultStory = () => ({
  components: { About },
  template: '<About />'
})
