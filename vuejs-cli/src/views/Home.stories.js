import Home from './Home.vue'

export default {
  component: Home,
  title: 'views/Home'
}

export const defaultStoryEn = () => ({
  components: { Home },
  template: '<Home />',
  mounted () {
    this.$i18n.locale = 'en'
  }
})

export const defaultStoryFr = () => ({
  components: { Home },
  template: '<Home />',
  mounted () {
    this.$i18n.locale = 'fr'
  }
})
