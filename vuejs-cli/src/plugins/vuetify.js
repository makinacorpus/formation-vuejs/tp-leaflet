import Vue from 'vue'
import Vuetify from 'vuetify'

Vue.use(Vuetify)

export const options = {
  theme: {
    dark: true,
    themes: {
      dark: {
        primary: '#bbbf12',
        secondary: '#404040',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107'
      }
    }
  }
}

export default new Vuetify(options)
