import Vue from 'vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'

import i18n from '../src/i18n'
import { options } from '../src/plugins/vuetify'
import router from '../src/router'

i18n.locale = 'en'
i18n.fallbackLocale = 'en'

Vue.use(VueRouter)
Vue.use(Vuetify)
const vuetify = new Vuetify(options)

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' }
}

// THIS is my decorator
export const decorators = [
  (story, context) => {
    // wrap the passed component within the passed context
    const wrapped = story(context)
    // extend Vue to use Vuetify around the wrapped component
    return Vue.extend({
      vuetify,
      i18n,
      router,
      components: { wrapped },
      template: `
        <v-app dark>
          <v-main app dark>
            <v-container fluid pa-0>
              <wrapped />
            </v-container>
          </v-main>
        </v-app>
      `
    })
  }
]
